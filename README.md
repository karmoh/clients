# Clients

## Running application

```
gradlew bootRun
```

Go to [Swagger](http://localhost:8080/swagger-ui.html) resources

Go to http://localhost:8080/ for application

login info

username:password

``
juss
``:``
superStrongPassword1
``

``
jaanus
``:``
notSooSecure
``

``
pets
``:``
notPassword
``
### Prerequisites

You will need Java 13

## Running the tests

```
gradlew test
```

## Built With

* Java13
* Spring
* Liquibase
* Lombok
* SpringFox
* JUnit
* Mockito
* REST Assured
