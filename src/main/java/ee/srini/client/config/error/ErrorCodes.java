package ee.srini.client.config.error;

public enum ErrorCodes {
  TECHNICAL_EXCEPTION,
  RESOURCE_NOT_FOUND,
  PARAM_REQUIRED,
  NOT_AUTHORIZED_EXCEPTION;

  public String getErrorCode() {
    return this.name().toLowerCase();
  }

}
