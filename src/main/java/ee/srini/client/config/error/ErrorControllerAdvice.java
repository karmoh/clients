package ee.srini.client.config.error;

import ee.srini.client.config.error.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.invoke.MethodHandles;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.*;

@ControllerAdvice
@RequestMapping(produces = "application/json")
public class ErrorControllerAdvice {
  private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<ErrorMessage> notFoundException(final ResourceNotFoundException e) {
    return error(e, ErrorCodes.RESOURCE_NOT_FOUND.getErrorCode(), HttpStatus.NOT_FOUND, e.getArguments());
  }

  @ExceptionHandler(BusinessException.class)
  public ResponseEntity<ErrorMessage> businessException(final BusinessException e) {
    return error(e, e.getMessage(), HttpStatus.NOT_ACCEPTABLE, e.getArguments());
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ErrorMessage> bindException(final MethodArgumentNotValidException e) {
    List<String> errors = new ArrayList<>();
    e.getBindingResult().getAllErrors().forEach(fieldError -> {
      FieldError error = (FieldError) fieldError;
      errors.add(error.getField());
    });
    return error(e, ErrorCodes.PARAM_REQUIRED.getErrorCode(), HttpStatus.NOT_ACCEPTABLE, errors);
  }

  @ExceptionHandler(BindException.class)
  public ResponseEntity<ErrorMessage> bindException(final BindException e) {
    List<String> errors = new ArrayList<>();
    e.getBindingResult().getAllErrors().forEach(fieldError -> {
      FieldError error = (FieldError) fieldError;
      errors.add(error.getField());
    });
    return error(e, ErrorCodes.PARAM_REQUIRED.getErrorCode(), HttpStatus.NOT_ACCEPTABLE, errors.toArray());
  }

  @ExceptionHandler({AccessDeniedException.class, NotAuthorizedException.class, AuthenticationCredentialsNotFoundException.class})
  public ResponseEntity<ErrorMessage> notAuthorizedException(final Exception e) {
    if (e instanceof NotAuthorizedException) {
      return error(e, e.getMessage(), HttpStatus.UNAUTHORIZED, ((NotAuthorizedException) e).getArguments());
    }
    return error(e, e.getMessage(), HttpStatus.UNAUTHORIZED);
  }

  @ExceptionHandler(ErrorMessageException.class)
  public ResponseEntity<ErrorMessage> errorMessageException(final ErrorMessageException exception) {
    LOGGER.error(exception.getErrorMessage().toString(), exception);
    return new ResponseEntity<>(exception.getErrorMessage(), Objects.requireNonNull(HttpStatus.resolve(exception.getStatusCode())));
  }

  @ExceptionHandler({TechnicalException.class, Exception.class})
  public ResponseEntity<ErrorMessage> technicalException(final Exception exception) {
    String uuid = UUID.randomUUID().toString();
    ErrorMessage responseMessage = new ErrorMessage(uuid, ErrorCodes.TECHNICAL_EXCEPTION.getErrorCode(), LocalDateTime.now(Clock.systemUTC()), null);
    LOGGER.error(responseMessage.toString(), exception);
    return new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
  }


  private ResponseEntity<ErrorMessage> error(
    final Exception exception,
    String messageCode,
    HttpStatus httpStatus,
    Object... arguments) {
    ErrorMessage responseMessage = new ErrorMessage(UUID.randomUUID().toString(), messageCode, LocalDateTime.now(Clock.systemUTC()), arguments);
    LOGGER.error(responseMessage.toString(), exception);
    return new ResponseEntity<>(responseMessage, httpStatus);
  }
}
