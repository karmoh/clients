package ee.srini.client.config.error;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ee.srini.client.config.serializer.LocalDateTimeSerializer;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ErrorMessage {
  private String ref;
  private String errorCode;
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  private LocalDateTime dateTime;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private Object[] arguments;

}
