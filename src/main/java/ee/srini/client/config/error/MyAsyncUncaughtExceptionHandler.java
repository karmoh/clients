package ee.srini.client.config.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Method;

public class MyAsyncUncaughtExceptionHandler implements AsyncUncaughtExceptionHandler {
  private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

  @Override
  public void handleUncaughtException(Throwable ex, Method method, Object... params) {
    LOGGER.error("Method Name::{}", method.getName());
    LOGGER.error("Exception occurred::{}", ex);
  }
}
