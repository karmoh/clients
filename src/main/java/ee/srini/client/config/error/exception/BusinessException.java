package ee.srini.client.config.error.exception;


import ee.srini.client.config.error.ErrorCodes;

public class BusinessException extends RuntimeException {

  private final Object[] arguments;

  public BusinessException(ErrorCodes message, Object... arguments) {
    super(message.getErrorCode());
    this.arguments = arguments;
  }

  public Object[] getArguments() {
    return arguments;
  }
}
