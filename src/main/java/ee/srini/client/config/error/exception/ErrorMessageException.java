package ee.srini.client.config.error.exception;


import ee.srini.client.config.error.ErrorMessage;

public class ErrorMessageException extends RuntimeException {

  private final ErrorMessage errorMessage;
  private final int statusCode;


  public ErrorMessageException(Throwable cause, ErrorMessage errorMessage, int statusCode) {
    super(cause);
    this.errorMessage = errorMessage;
    this.statusCode = statusCode;
  }

  public int getStatusCode() {
    return statusCode;
  }

  public ErrorMessage getErrorMessage() {
    return errorMessage;
  }
}
