package ee.srini.client.config.error.exception;


import ee.srini.client.config.error.ErrorCodes;

public class NotAuthorizedException extends RuntimeException {

  private final Object[] arguments;


  public NotAuthorizedException() {
    super(ErrorCodes.NOT_AUTHORIZED_EXCEPTION.getErrorCode());
    arguments = null;
  }

  public NotAuthorizedException(ErrorCodes message, Object... arguments) {
    super(message.getErrorCode());
    this.arguments = arguments;
  }

  public Object[] getArguments() {
    return arguments;
  }
}
