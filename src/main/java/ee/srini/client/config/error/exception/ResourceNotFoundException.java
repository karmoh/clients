package ee.srini.client.config.error.exception;

public class ResourceNotFoundException extends RuntimeException {
  private final Object[] arguments;

  public ResourceNotFoundException(Object... arguments) {
    super();
    this.arguments = arguments;
  }

  public Object[] getArguments() {
    return arguments;
  }
}
