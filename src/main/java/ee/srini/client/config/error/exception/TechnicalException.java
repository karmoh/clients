package ee.srini.client.config.error.exception;

public class TechnicalException extends RuntimeException {

  public TechnicalException(Throwable cause) {
    super(cause);
  }

  public TechnicalException(String message) {
    super(message);
  }
}
