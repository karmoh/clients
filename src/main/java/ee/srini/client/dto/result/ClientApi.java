package ee.srini.client.dto.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClientApi {
    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String address;
    private Long countryId;
    private String countryName;
}
