package ee.srini.client.service;

import ee.srini.client.config.error.exception.NotAuthorizedException;
import ee.srini.client.config.error.exception.ResourceNotFoundException;
import ee.srini.client.dto.request.ClientRequest;
import ee.srini.client.dto.result.SimpleClientApi;
import ee.srini.client.entity.ApplicationUserEntity;
import ee.srini.client.entity.ClientEntity;
import ee.srini.client.repository.ApplicationUserRepository;
import ee.srini.client.repository.ClientRepository;
import ee.srini.client.repository.CountryRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ClientService {

    private final ClientRepository clientRepository;
    private final ApplicationUserRepository applicationUserRepository;
    private final CountryRepository countryRepository;


    public List<SimpleClientApi> getLoginUserClients() {
        ApplicationUserEntity loginUser = getLoginUser();
        return clientRepository.findByUserIdOrderByUserIdDesc(loginUser.getId())
                .stream()
                .map(this::mapClientDataToSimpleApi)
                .collect(Collectors.toList());
    }

    private SimpleClientApi mapClientDataToSimpleApi(ClientEntity clientEntity) {
        return SimpleClientApi.builder()
                .id(clientEntity.getId())
                .firstName(clientEntity.getFirstName())
                .lastName(clientEntity.getLastName())
                .username(clientEntity.getUsername())
                .build();
    }

    public ClientEntity getLoginUserClientDetails(Long clientId) {
        ApplicationUserEntity loginUser = getLoginUser();
        ClientEntity clientEntity = getClientById(clientId);
        if (!loginUser.getId().equals(clientEntity.getUserId())) {
            throw new ResourceNotFoundException("Client", clientId);
        }
        return clientEntity;
    }

    private ClientEntity getClientById(Long id) {
        return clientRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Client", id));
    }

    private ApplicationUserEntity getLoginUser() {
        SecurityContext context = SecurityContextHolder.getContext();
        UserDetails userDetails = (UserDetails) context.getAuthentication().getPrincipal();
        return applicationUserRepository.findByUsername(userDetails.getUsername()).orElseThrow(NotAuthorizedException::new);
    }

    public Long createUpdateClient(ClientRequest request) {
        ApplicationUserEntity loginUser = getLoginUser();
        ClientEntity clientEntity;
        if (request.getId() != null) {
            clientEntity = getClientById(request.getId());
            if (!loginUser.getId().equals(clientEntity.getUserId())) {
                throw new ResourceNotFoundException("Client", request.getId());
            }
        } else {
            clientEntity = ClientEntity.builder().build();
        }

        validateCountryId(request.getCountry());

        clientEntity.setFirstName(request.getFirstName());
        clientEntity.setLastName(request.getLastName());
        clientEntity.setUsername(request.getUsername());
        clientEntity.setEmail(request.getEmail());
        clientEntity.setUserId(loginUser.getId());
        clientEntity.setAddress(request.getAddress());
        clientEntity.setCountryId(request.getCountry());
        clientRepository.save(clientEntity);
        return clientEntity.getId();
    }

    private void validateCountryId(Long id) {
        countryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Country", id));
    }
}
