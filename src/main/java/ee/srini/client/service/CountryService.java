package ee.srini.client.service;

import ee.srini.client.entity.CountryEntity;
import ee.srini.client.repository.CountryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@Service
@AllArgsConstructor
public class CountryService {

    private final CountryRepository countryRepository;


    @Cacheable("countries")
    public List<CountryEntity> getCountries() {
        log.info("Getting countries");
        return countryRepository.findAll(Sort.by("id"));
    }
}
