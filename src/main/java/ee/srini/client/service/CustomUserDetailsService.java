package ee.srini.client.service;

import ee.srini.client.entity.ApplicationUserEntity;
import ee.srini.client.repository.ApplicationUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@AllArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final ApplicationUserRepository applicationUserRepository;


    @Override
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {
        ApplicationUserEntity result = applicationUserRepository
                .findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        return new User(result.getUsername(), result.getPassword(), Collections.emptyList());
    }

}
