package ee.srini.client.web;

import ee.srini.client.dto.ServiceResponse;
import ee.srini.client.dto.request.ClientRequest;
import ee.srini.client.dto.result.SimpleClientApi;
import ee.srini.client.entity.ClientEntity;
import ee.srini.client.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
public class ClientController {

    private final ClientService clientService;


    @GetMapping("clients")
    public ServiceResponse<List<SimpleClientApi>> getLoginUserClients() {
        List<SimpleClientApi> result = clientService.getLoginUserClients();
        return ServiceResponse.ok(result, result.size());
    }

    @GetMapping("clients/{id}")
    public ServiceResponse<ClientEntity> getLoginUserClientDetails(@PathVariable Long id) {
        return ServiceResponse.ok(clientService.getLoginUserClientDetails(id));
    }

    @PostMapping("clients")
    public ServiceResponse<Long> createUpdateClient(@Valid @RequestBody ClientRequest request) {
        return ServiceResponse.ok(clientService.createUpdateClient(request));
    }
}
