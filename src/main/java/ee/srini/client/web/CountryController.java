package ee.srini.client.web;

import ee.srini.client.dto.ServiceResponse;
import ee.srini.client.entity.CountryEntity;
import ee.srini.client.service.CountryService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class CountryController {

    private final CountryService countryService;


    @GetMapping("countries")
    public ServiceResponse<List<CountryEntity>> getCountries() {
        List<CountryEntity> result = countryService.getCountries();
        return ServiceResponse.ok(result, result.size());
    }
}

