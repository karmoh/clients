--liquibase formatted sql
--encoding check: [šžõäöü], [ŠŽÕÄÖÜ]

--changeset karmo.hiis@gmail.com:20200414-01 context:prod

create sequence hibernate_sequence minvalue 10000;

create table application_user
(
    id       bigint auto_increment,
    username varchar(255) not null unique,
    password varchar(250) not null,
    primary key (id)
);

create table client
(
    id         bigint auto_increment,
    user_id    bigint       not null,
    first_name varchar(255) not null,
    last_name  varchar(255) not null,
    username   varchar(255) not null,
    email      varchar(255),
    address    varchar(255) not null,
    country_id bigint       not null,
    primary key (id)
);

create table country
(
    id   bigint auto_increment,
    name varchar(255) not null,
    primary key (id)
);

create index idx_user_user_name
    on application_user (username);
create index idx_client_user
    on client (user_id);

alter table client
    add constraint client_user
        foreign key (user_id) references application_user (id);

insert into application_user (id, username, password)
values (1, 'juss', '$2a$10$a4sRFj83yulHbmU7tr8mnuW0HcIzRdGw2bgcqN3.LP.BvG5xcPCgq'),--superStrongPassword1
       (2, 'jaanus', '$2a$10$GPyXIDUXiiP5gP5kWkdbA.bjrOlpdtHkcaFDQW1I50oKpcyDlOipy'),--notSooSecure
       (3, 'pets', '$2a$10$G7q55cQW4eWB5EYXU1Qv0.VYvdbTGz.JaglnkN3K7YOcKl8ZSkp6O');--notPassword

insert into country (id, name)
values (1, 'Estonia'),
       (2, 'Latvia'),
       (3, 'Lithuania'),
       (4, 'Finland'),
       (5, 'Sweden');

insert into client (id, user_id, first_name, last_name, username, email, address, country_id)
values (1, 1, 'Martin', 'Romero', 'momero', 'martin.romero@somemail.com', 'address1', 4),
       (2, 2, 'Johnny', 'Rhodes', 'Johnny2', 'Johnny2@somemail.com', 'address2', 2),
       (3, 3, 'Cassandra', 'Lyons', 'lyonssss', 'lyonssss@somemail.com', 'address3', 3);