--liquibase formatted sql
--encoding check: [šžõäöü], [ŠŽÕÄÖÜ]

--changeSet karmo.hiis@gmail.com:load_dev_data runOnChange:true runAlways:true context:dev labels:data
delete
from client;
delete
from country;
delete
from application_user;
insert into application_user (id, username, password)
values (1, 'juss', 'pass');

insert into country (id, name)
values (1, 'Estonia'),
       (2, 'Latvia'),
       (3, 'Lithuania'),
       (4, 'Finland'),
       (5, 'Sweden');

insert into client (id, user_id, first_name, last_name, username, email, address, country_id)
values (1, 1, 'Martin', 'Romero', 'momero', 'martin.romero@somemail.com', 'address1', 4);