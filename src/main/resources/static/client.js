let dropdown = $('#country-select');
dropdown.empty();
const countriesUrl = '/countries';

// Populate dropdown
$.getJSON(countriesUrl, function (data) {
    $.each(data.data, function (key, entry) {
        dropdown.append($('<option></option>').attr('value', entry.id).text(entry.name));
    })
});

$("#client-form").submit(function (event) {
    event.preventDefault();
    var json = ConvertFormToJSON($(this));

    $.ajax({
        type: "POST",
        data: JSON.stringify(json),
        contentType: "application/json",
        dataType: "json",
        cache: false,
        url: "/clients",
        success: function () {
            location.replace("/");
        }
    });

    return true;
});

//Add client data to form
const clientUrl = '/clients';
const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get('id');
if (id != null) {
    fillForm(id);
    $('.alter-text').html("Edit Client");
}

function fillForm(id) {

    const getClientUrl = clientUrl + "/" + id;
    $.getJSON(getClientUrl, function (data) {
        $('#client-id').val(data.data.id);
        $('#firstName').val(data.data.firstName);
        $('#lastName').val(data.data.lastName);
        $('#username').val(data.data.username);
        $('#email').val(data.data.email);
        $('#address').val(data.data.address);
        $('#country-select').val(data.data.countryId);
    });

}

function ConvertFormToJSON(form) {
    var array = jQuery(form).serializeArray();
    var json = {};
    jQuery.each(array, function () {
        json[this.name] = this.value || '';
    });
    return json;
}
