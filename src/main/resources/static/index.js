const clientUrl = '/clients';

//Populate table with data
$.getJSON(clientUrl, function (data) {
    var table = $('.table tbody');
    data.data.forEach(function (d) {
        var markup = "<tr><td>" + d.id + "</td><td>" + d.firstName + "</td><td>" + d.lastName + "</td><td>" +
            d.username + "</td><td><a href=\"client.html?id=" + d.id + "\" class=\"btn btn-primary\">Edit client</a></td></tr>";
        table.append(markup);
    });
});



