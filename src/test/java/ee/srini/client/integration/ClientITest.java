package ee.srini.client.integration;

import ee.srini.client.dto.request.ClientRequest;
import io.restassured.http.ContentType;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.hamcrest.Matchers.equalTo;

class ClientITest extends IntegrationTestBase {
    private static final String SERVICE_PATH = "clients";

    @Test
    void getLoginUserClients() {
        SecurityContextHolder.getContext().setAuthentication(createAuthentication());
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(1))
                .body("data[0].id", equalTo(1))
                .body("data[0].firstName", equalTo("Martin"))
                .body("data[0].lastName", equalTo("Romero"))
                .body("data[0].username", equalTo("momero"));
    }

    @Test
    void getLoginUserClientDetails() {
        SecurityContextHolder.getContext().setAuthentication(createAuthentication());
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH + "/{0}", 1)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data.id", equalTo(1))
                .body("data.userId", equalTo(1))
                .body("data.firstName", equalTo("Martin"))
                .body("data.lastName", equalTo("Romero"))
                .body("data.username", equalTo("momero"))
                .body("data.email", equalTo("martin.romero@somemail.com"))
                .body("data.address", equalTo("address1"))
                .body("data.countryId", equalTo(4));
    }

    @Test
    void createUpdateClientCreate() {
        ClientRequest request = ClientRequest.builder()
                .firstName("firstName")
                .lastName("lastName")
                .username("username")
                .email("email")
                .address("address")
                .country(1L)
                .build();

        SecurityContextHolder.getContext().setAuthentication(createAuthentication());
        RestAssuredMockMvc
                .given()
                .body(request)
                .contentType(ContentType.JSON)
                .when()
                .post(SERVICE_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("data", equalTo(10000));
        SecurityContextHolder.getContext().setAuthentication(createAuthentication());
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH + "/{0}", 10000)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data.id", equalTo(10000))
                .body("data.userId", equalTo(1))
                .body("data.firstName", equalTo("firstName"))
                .body("data.lastName", equalTo("lastName"))
                .body("data.username", equalTo("username"))
                .body("data.email", equalTo("email"))
                .body("data.address", equalTo("address"))
                .body("data.countryId", equalTo(1));
    }

    @Test
    void createUpdateClientUpdate() {
        ClientRequest request = ClientRequest.builder()
                .id(1L)
                .firstName("firstName")
                .lastName("lastName")
                .username("username")
                .email("email")
                .address("address")
                .country(1L)
                .build();

        SecurityContextHolder.getContext().setAuthentication(createAuthentication());
        RestAssuredMockMvc
                .given()
                .body(request)
                .contentType(ContentType.JSON)
                .when()
                .post(SERVICE_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("data", equalTo(1));
        SecurityContextHolder.getContext().setAuthentication(createAuthentication());
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH + "/{0}", 1)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data.id", equalTo(1))
                .body("data.userId", equalTo(1))
                .body("data.firstName", equalTo("firstName"))
                .body("data.lastName", equalTo("lastName"))
                .body("data.username", equalTo("username"))
                .body("data.email", equalTo("email"))
                .body("data.address", equalTo("address"))
                .body("data.countryId", equalTo(1));
    }

}
