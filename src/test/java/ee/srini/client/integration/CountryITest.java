package ee.srini.client.integration;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.hamcrest.Matchers.equalTo;

class CountryITest extends IntegrationTestBase {
    private static final String SERVICE_PATH = "countries";

    @Test
    void getCountries() {
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("total", equalTo(5))
                .body("data[0].id", equalTo(1))
                .body("data[0].name", equalTo("Estonia"))
                .body("data[4].id", equalTo(5))
                .body("data[4].name", equalTo("Sweden"));
    }


}
