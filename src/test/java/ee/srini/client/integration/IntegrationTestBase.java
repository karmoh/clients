package ee.srini.client.integration;

import ee.srini.client.ClientApplication;
import io.restassured.config.DecoderConfig;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import io.restassured.module.mockmvc.config.RestAssuredMockMvcConfig;
import liquibase.integration.spring.SpringLiquibase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.context.WebApplicationContext;

import java.util.Collections;


@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = ClientApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public abstract class IntegrationTestBase {

    @Autowired
    private WebApplicationContext context;


    @Autowired
    private SpringLiquibase liquibase;


    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        RestAssuredMockMvc.config = RestAssuredMockMvcConfig.config().decoderConfig(DecoderConfig.decoderConfig().defaultContentCharset("UTF-8"));
        RestAssuredMockMvc.webAppContextSetup(context);
        initTestData();
    }

    @AfterEach
    public void cleanUp() {
        RestAssuredMockMvc.reset();
    }

    private void initTestData() throws Exception {
        liquibase.setContexts("prod,dev");
        liquibase.setShouldRun(true);
        liquibase.afterPropertiesSet();
    }

    Authentication createAuthentication() {
        TestingAuthenticationToken testAuth = new TestingAuthenticationToken(
                new User("juss", "password", Collections.emptyList()),
                "userCredentials");
        testAuth.setAuthenticated(true);
        return testAuth;
    }

}
