package ee.srini.client.service;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Collections;

public abstract class AbstractServiceTest<T> {

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        SecurityContextHolder.getContext().setAuthentication(createAuthentication());
    }

    private Authentication createAuthentication() {
        TestingAuthenticationToken testAuth =
                new TestingAuthenticationToken(
                        new User("username", "password", Collections.emptyList()), "userCredentials");
        testAuth.setAuthenticated(true);
        return testAuth;
    }

    protected abstract T createService();
}
