package ee.srini.client.service;

import ee.srini.client.dto.request.ClientRequest;
import ee.srini.client.dto.result.SimpleClientApi;
import ee.srini.client.entity.ApplicationUserEntity;
import ee.srini.client.entity.ClientEntity;
import ee.srini.client.entity.CountryEntity;
import ee.srini.client.repository.ApplicationUserRepository;
import ee.srini.client.repository.ClientRepository;
import ee.srini.client.repository.CountryRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ClientServiceTest extends AbstractServiceTest<ClientService> {

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ApplicationUserRepository applicationUserRepository;
    @Mock
    private CountryRepository countryRepository;

    @Mock
    private ClientEntity clientEntity;
    @Mock
    private CountryEntity countryEntity;

    @Captor
    private ArgumentCaptor<ClientEntity> clientEntityArgumentCaptor;

    @Test
    void getLoginUserClients() {
        when(applicationUserRepository.findByUsername("username")).thenReturn(Optional.of(ApplicationUserEntity.builder()
                .id(1L)
                .build()));
        ClientEntity clientEntity = ClientEntity.builder()
                .id(2L)
                .firstName("firstName")
                .lastName("lastName")
                .username("username")
                .build();
        when(clientRepository.findByUserIdOrderByUserIdDesc(1L)).thenReturn(Collections.singletonList(clientEntity));
        List<SimpleClientApi> result = createService().getLoginUserClients();
        SimpleClientApi simpleClientApi = result.get(0);
        assertEquals(clientEntity.getId(), simpleClientApi.getId());
        assertEquals(clientEntity.getFirstName(), simpleClientApi.getFirstName());
        assertEquals(clientEntity.getLastName(), simpleClientApi.getLastName());
        assertEquals(clientEntity.getUsername(), simpleClientApi.getUsername());
    }

    @Test
    void getLoginUserClientDetails() {
        when(applicationUserRepository.findByUsername("username")).thenReturn(Optional.of(ApplicationUserEntity.builder()
                .id(2L)
                .build()));
        when(clientRepository.findById(1L)).thenReturn(Optional.of(clientEntity));
        when(clientEntity.getUserId()).thenReturn(2L);
        assertEquals(clientEntity, createService().getLoginUserClientDetails(1L));
    }

    @Test
    void createUpdateClientCreate() {
        when(applicationUserRepository.findByUsername("username")).thenReturn(Optional.of(ApplicationUserEntity.builder()
                .id(2L)
                .build()));
        when(countryRepository.findById(1L)).thenReturn(Optional.of(countryEntity));
        ClientRequest request = ClientRequest.builder()
                .firstName("firstName")
                .lastName("lastName")
                .username("username")
                .email("email")
                .address("address")
                .country(1L)
                .build();
        createService().createUpdateClient(request);
        verify(clientRepository).save(clientEntityArgumentCaptor.capture());
        ClientEntity clientEntity = clientEntityArgumentCaptor.getValue();
        assertEquals(2L, clientEntity.getUserId());
        assertEquals(request.getFirstName(), clientEntity.getFirstName());
        assertEquals(request.getLastName(), clientEntity.getLastName());
        assertEquals(request.getUsername(), clientEntity.getUsername());
        assertEquals(request.getEmail(), clientEntity.getEmail());
        assertEquals(request.getAddress(), clientEntity.getAddress());
        assertEquals(request.getCountry(), clientEntity.getCountryId());
    }

    @Test
    void createUpdateClientUpdate() {
        when(applicationUserRepository.findByUsername("username")).thenReturn(Optional.of(ApplicationUserEntity.builder()
                .id(2L)
                .build()));
        when(countryRepository.findById(1L)).thenReturn(Optional.of(countryEntity));
        when(clientRepository.findById(3L)).thenReturn(Optional.of(clientEntity));
        when(clientEntity.getUserId()).thenReturn(2L);
        ClientRequest request = ClientRequest.builder()
                .id(3L)
                .firstName("firstName")
                .lastName("lastName")
                .username("username")
                .email("email")
                .address("address")
                .country(1L)
                .build();

        createService().createUpdateClient(request);
        verify(clientRepository).save(clientEntity);
        verify(clientEntity).setUserId(2L);
        verify(clientEntity).setFirstName(request.getFirstName());
        verify(clientEntity).setLastName(request.getLastName());
        verify(clientEntity).setUsername(request.getUsername());
        verify(clientEntity).setEmail(request.getEmail());
        verify(clientEntity).setAddress(request.getAddress());
        verify(clientEntity).setCountryId(request.getCountry());

    }

    @Override
    protected ClientService createService() {
        return new ClientService(clientRepository, applicationUserRepository, countryRepository);
    }
}