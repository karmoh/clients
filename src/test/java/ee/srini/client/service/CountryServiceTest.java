package ee.srini.client.service;

import ee.srini.client.entity.CountryEntity;
import ee.srini.client.repository.CountryRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.data.domain.Sort;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CountryServiceTest extends AbstractServiceTest<CountryService> {

    @Mock
    private CountryRepository countryRepository;

    @Mock
    private CountryEntity countryEntity;

    @Test
    void getCountries() {
        when(countryRepository.findAll(Sort.by("id"))).thenReturn(Collections.singletonList(countryEntity));
        assertEquals(countryEntity,createService().getCountries().get(0));
    }

    @Override
    protected CountryService createService() {
        return new CountryService(countryRepository);
    }
}