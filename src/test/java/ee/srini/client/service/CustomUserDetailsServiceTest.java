package ee.srini.client.service;

import ee.srini.client.entity.ApplicationUserEntity;
import ee.srini.client.repository.ApplicationUserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class CustomUserDetailsServiceTest extends AbstractServiceTest<CustomUserDetailsService> {

    @Mock
    private ApplicationUserRepository applicationUserRepository;

    @Test
    void loadUserByUsername() {
        when(applicationUserRepository.findByUsername("userName")).thenReturn(Optional.of(ApplicationUserEntity.builder()
                .username("username")
                .password("password")
                .build()));
        UserDetails result = createService().loadUserByUsername("userName");
        assertEquals("username", result.getUsername());
        assertEquals("password", result.getPassword());
    }

    @Override
    protected CustomUserDetailsService createService() {
        return new CustomUserDetailsService(applicationUserRepository);
    }
}